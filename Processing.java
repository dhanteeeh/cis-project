
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

/**
 * This class provide method to pre-process the input
 * 
 * @author fuaida
 */
public class Processing {

	public static final int BLOCK_SIZE = 16;

	/**
	 * This method is used to process file input into byte[]
	 * 
	 * @param file
	 * @return plaintext in byte
	 */
	public static byte[] plainInputProcessing(File file) {
		byte[] plainByte = null;

		try {
			plainByte = Files.readAllBytes(file.toPath());

		} catch (Exception e) {
			System.out.println("Input Error: " + e.getMessage());
		}
		return plainByte;
	}

	/**
	 * This method is used to process file key into byte[]
	 * 
	 * @param file
	 * @return
	 */
	public static byte[] keyInputProcessing(File file) {
		String key;
		byte[] keyByte = null;
		try {
			byte[] bytes = Files.readAllBytes(file.toPath());
			key = new String(bytes, "UTF-8");
			keyByte = hexToBytes(key);
		} catch (Exception e) {
			System.out.println("Key Error: " + e.getMessage());
		}
		return keyByte;
	}

	/**
	 * This method is used to write the result byte from AES-CTR Calculator into
	 * output file
	 * 
	 * @param result
	 * @param file
	 * @throws IOException
	 */
	public static void outputProcessing(byte[] result, File file) throws IOException {
		FileOutputStream stream;
		try {
			stream = new FileOutputStream(file);

			byte[] output = new byte[result.length];
			for (int i = 0; i < result.length; i++) {
				output[i] = (byte) (result[i] - 128);
			}
			stream.write(output);
			stream.close();
		} catch (IOException e) {
			System.out.println("Output Error: " + e.getMessage());
		}
	}

	/**
	 * This method is used to convert string(hex) into byte[]
	 * 
	 * @param hexBytes:
	 *            Hexabyte in string
	 * @return
	 */
	public static final byte[] hexToBytes(final String hexBytes) {
		if (hexBytes == null | hexBytes.length() < 2) {
			return null;
		} else {
			int length = hexBytes.length() / 2;
			byte[] buffer = new byte[length];
			for (int i = 0; i < length; i++) {
				buffer[i] = (byte) Integer.parseInt(hexBytes.substring(i * 2, i * 2 + 2), 16);
			}
			return buffer;
		}
	}

}
