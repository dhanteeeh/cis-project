
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class implements CTR mode for AES encryption and decryption
 * 
 * @author ramadhanty
 */

public class CTR {

	/**
	 * Default IV used for encryption and decryption
	 */
	public final static byte[] IV_BYTES = Processing.hexToBytes("006CB6DBC0543B59DA48D90B00000001");
	public final static IvParameterSpec IV_SPEC = new IvParameterSpec(IV_BYTES);

	/**
	 * This method is used to encrypt the plaintext to ciphertext with the key
	 * provided.
	 * 
	 * @param plain:
	 *            the plaintext in byte
	 * @param key:
	 *            the key in byte
	 * @return encrypted plaintext in byte
	 */
	public static byte[] encrypt(byte[] plain, byte[] key) {
		byte[] result = new byte[16384];
		try {
			SecretKey AESkey = new SecretKeySpec(key, "AES");
			Cipher cipherEncrypt = Cipher.getInstance("AES/CTR/PKCS5Padding");
			cipherEncrypt.init(Cipher.ENCRYPT_MODE, AESkey, IV_SPEC);
			result = cipherEncrypt.doFinal((plain));
		} catch (Exception e) {
			System.out.println("Encryption Error: " + e.getMessage());
		}
		return result;
	}

	/**
	 * This method is used to decrypt the ciphertext into plaintext with the key
	 * provided.
	 * 
	 * @param cipher:
	 *            the ciphertext in byte
	 * @param key:
	 *            the key in byte
	 * @return decrypted plaintext in byte
	 */

	public static byte[] decrypt(byte[] cipher, byte[] key) {

		byte[] result = new byte[16384];
		try {
			SecretKey AESkey = new SecretKeySpec(key, "AES");
			Cipher cipherDecrypt = Cipher.getInstance("AES/CTR/PKCS5Padding");
			cipherDecrypt.init(Cipher.DECRYPT_MODE, AESkey, IV_SPEC);

			result = cipherDecrypt.doFinal((cipher));
		} catch (Exception e) {
			System.out.println("Decryption Error: " + e.getMessage());
		}
		return result;
	}

	/**
	 * This method is used to check if the input key is valid or not. Key is
	 * considered valid if its length is 16 byte/24 byte/32 byte
	 * 
	 * @param data:
	 *            the input key in byte
	 * @return true if the key is valid, false otherwise
	 * 
	 */
	public static boolean isValidKey(byte[] data) {
		int dataSize = data.length;
		boolean status;
		if (dataSize == 16 || dataSize == 24 || dataSize == 32) {
			status = true;
		} else {
			status = false;
		}
		return status;
	}

}
