# AES-CTR MODE CALCULATOR

This software implement the encryption and decryption in Advanced Encryption Standard(AES) with Counter(CTR) mode. AES-CTR Mode Calc is using PKCS#5 to padding the plaintext that is input to the software. This calculator will only accept Key in 128 bits(16 bytes), 192 bits(24 bytes) or 256 bits(32 bytes). 

## Library and Package
---
* [Java Cryptography Extension (JCE)](http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html) - Standard Extension of Java Cryptography Architecture.
* [Javax.Crypto](https://docs.oracle.com/javase/7/docs/api/javax/crypto/package-summary.html) - Java Packages Include Classes and Interfaces for Cryptographic Operations.
* [Java.Io](https://docs.oracle.com/javase/7/docs/api/java/io/package-summary.html) - Java Package for System Input and Output through Data Streams, Serialization and the File System.
* [Javax.Swing](https://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html) - Java Package for Creating GUI Program.

## How to User AES-CTR Mode Calc
---
### First Feature: Encrypt
Do this steps:

```sh
 - Open the AES-CTR-Calc.jar
 - Browse your key file on the first field
 - Browse your plaintext file on the second field
 - Click "Encrypt" button 
 - You will find your ciphertext in file encrypt_(your-input-file-name)
```

### Second Feature: Decrypt
Do this steps:
```sh
 - Open the AES-CTR-Calc.jar
 - Browse your key file on the first field
 - Browse your ciphertext file on the second field
 - Click "Decrypt" button. 
 - You will find your ciphertext in file decrypt_(your-input-file-name).
```